# Description

Typescript API library for communicate with CryptoSocket application.

## Startup

1. Install CryptoSocker application.

2. Install npm package using `npm install @usrt/cryptosocket-wrapper`.

3. Type next script:

```ts
// Create client
var client = new CryptoSocketClient();

// Connect to CryptoSocket on your computer
await client.connect('CRYPTOSOCKET_API_TOKEN');

// Select file on disk
const filePath = await client.showOpenDialog(
    'Please, select "*.p12" file on you pc!'
);

// If filepath if empty - user close select fiel window
if (!filePath) {
    return;
}

// Password of your p12 file key
const password = '12345678';

// Sign
```

# References

-   [Info about API](http://localhost:6128/)
-   [Link to script (HTTP)](http://localhost:6128/tumsocket.js)
-   [Link to script (HTTPS)](https://localhost:6129/tumsocket.js)

# Build source

1. Install dependencies using `npm install`.
2. Run build script `npm run build`.
