export interface ErrorData {
    message: string;
    code: string;
    data?: any;
}