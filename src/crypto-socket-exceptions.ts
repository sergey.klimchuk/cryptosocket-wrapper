export class CryptoSocketExceptions {
    public static readonly BROKEN_RESPONSE = 'BROKEN_RESPONSE';
    public static readonly INVALID_KEY = 'INVALID_KEY';
    public static readonly WRONG_PASSWORD = 'WRONG_PASSWORD';
    public static readonly API_TOKEN_EXPIRED = 'API_TOKEN_EXPIRED';
    public static readonly API_TOKEN_ERROR = 'API_TOKEN_ERROR';
    public static readonly UNKNOWN_ERROR = 'UNKNOWN_ERROR';
    public static readonly NOT_INIT = 'NOT_INIT';
}
