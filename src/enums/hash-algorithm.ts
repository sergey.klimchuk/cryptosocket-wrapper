/* eslint-disable no-unused-vars */
export enum HashAlgorithm {
    GOST_34_311 = 0,
    SHA1 = 1,
    SHA2 = 2,
    MD5 = 3,
}
