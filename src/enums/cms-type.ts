/* eslint-disable no-unused-vars */
export enum CmsType {
    DATA = 0,
    SIGN = 1,
    ENVELOPED = 2,
    DIGEST = 3,
    ENCRYPTED = 4,
    AUTHENTICATE = 5,
}
