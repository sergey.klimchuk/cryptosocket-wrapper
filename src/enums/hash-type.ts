/* eslint-disable no-unused-vars */
export enum HashType {
    SHA1 = 0,
    SHA256 = 1,
}
