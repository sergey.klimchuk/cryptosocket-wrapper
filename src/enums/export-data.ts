/* eslint-disable no-unused-vars */
export enum ExportData {
    CERTIFICATE_PEM = 0,
    KEY_PEM = 1,
    SSL_KEY_PEM = 2,
    SSL_SERVER_KEY_PEM = 3,
}
