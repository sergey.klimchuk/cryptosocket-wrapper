/* eslint-disable no-unused-vars */
export enum ExportDataToFileType {
    CERTIFICATE_PEM = 0,
    CERTIFICATE_DER = 1,
    KEY_PEM = 2,
    KEY_DER = 3,
    SSL_KEY_PEM = 4,
    SSL_KEY_DER = 5,
}
