/* eslint-disable no-unused-vars */
export enum HashFormat {
    BASE64 = 0,
    HEX_UPPER = 1,
    ALF = 2,
    HEX_LOWER = 3,
}
