/* eslint-disable no-unused-vars */
export enum ApiType {
    BaseAPI = 'BaseAPI',
    XMLAPI = 'XMLAPI',
    SYSAPI = 'SYSAPI',
    ASNAPI = 'ASNAPI',
    FORMAPI = 'FORMAPI',
}
