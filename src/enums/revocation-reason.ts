/* eslint-disable no-unused-vars */
export enum RevocationReason {
    UNSPECIFIED = 0,
    KEYCOMPROMISE = 1,
    CACOMPROMISE = 2,
    AFFILIATIONCHANGED = 3,
    SUPERSEDED = 4,
    CESSATIONOFOPERATION = 5,
    CERTIFICATEHOLD = 6,
    REMOVEFROMCRL = 8,
    PRIVILEGEWITHDRAWN = 9,
    AACOMPROMISE = 10,
}
