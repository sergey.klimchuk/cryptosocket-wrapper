export interface FingerprintInfo {
    dn: string;
    sn: string;
    indx: string;
    name: string;
    hash: string;
}
