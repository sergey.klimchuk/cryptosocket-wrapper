export interface Certificate {
    /**
     * Key algorithm id.
     */
    algID: string;
    /**
     * Field with certificate in base64.
     */
    certificateBlob: string;
    /**
     * Simple name of issuer ca.
     */
    issuerDN: string;
    /**
     * Public key in hex format.
     */
    keyBlob: string;
    /**
     * Key OID algorithm,
     */
    keyOid: string;
    /**
     * Profile of certificate key.
     */
    profile: string;
    /**
     * Certificate serial number.
     */
    serialNumber: string;
    /**
     * Simple name of subject.
     */
    subjectDN: string;
    /**
     * Key usages. (as code)
     */
    usage: string;
    /**
     * Extended key usages.
     */
    keyEx: string;
    /**
     * Certificate usage.
     */
    usageStr: string;
    /**
     * Start certificate life time.
     */
    validFrom: string;
    /**
     * Finish certificate life time.
     */
    validTo: string;
}