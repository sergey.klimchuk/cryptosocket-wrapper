export type IdCardInfo = string | {
    firstName: string;
    lastName: string;
    middleName: string;
    birthDate: string;
};