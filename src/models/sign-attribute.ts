export interface SignAttribute {
    value: string;
    oid: string;
    isBin: boolean;
}
