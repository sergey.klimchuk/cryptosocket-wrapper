export interface CertificateInfo {
    Subject: string;
    Issuer: string;
    Cert: string;
    NotBefore: string;
    NotAfter: string;
    SignAlg: string;
    CRLpoint: string;
    CApoint: string;
    IssSerNum: string;
    SerNum: string;
    KeyOID: string;
    PublicKey: string;
}
