import { SuccessResponse } from './responses';
import { ApiType } from './enums/api-type';
import { SignAttribute } from './models/sign-attribute';
import { mapSocketError } from './map-socket-error';
import { CryptoSocketExceptions } from './crypto-socket-exceptions';
import { Certificate } from './models/certificate';
import { RevocationReason } from './enums/revocation-reason';
import { IdCardInfo } from './models/id-card-info';
import { HashType } from './enums/hash-type';
import { HashFormat } from './enums/hash-format';
import { HashAlgorithm } from './enums/hash-algorithm';
import { ExportData } from './enums/export-data';
import { ExportDataToFileType } from './enums/export-data-to-file-type';
import { FingerprintInfo } from './models/fingerprint-info';
import { CertificateInfo } from './models/certificate-info';
import { CmsType } from './enums/cms-type';

export class CryptoSocketClient {
    private readonly addressToWSApplication = 'ws://localhost:6126/tumarcsp/';

    private socket: WebSocket | null = null;

    /**
     * Start connect with CryptoSocket.
     * @param apiKey key for active cryptosocket on domain.
     */
    async connect(apiKey?: string) {
        return new Promise<void>((resolve, _) => {
            this.socket = new WebSocket(this.addressToWSApplication);
            this.socket.onopen = async () => {
                if (!apiKey) {
                    return;
                }
                await this.setApiKey(apiKey);
                resolve();
            };
            this.socket.onclose = () => {
                this.socket = null;
            };
        });
    }

    async connectWithExistSocket(socket: WebSocket, apiKey?: string) {
        return new Promise<void>((resolve, _) => {
            this.socket = socket;
            this.socket.onopen = async () => {
                if (!apiKey) {
                    return;
                }
                await this.setApiKey(apiKey);
                resolve();
            };
            this.socket.onclose = () => {
                this.socket = null;
            };
        });
    }

    /**
     * Close connection.
     */
    close() {
        this.socket?.close();
    }

    /**
     * Set domain key.
     * Description: http://localhost:6128/SetAPIKey.html
     * @param apiKey key for active cryptosocket on domain.
     */
    setApiKey(apiKey: string) {
        return this.executeCommand<void>('SetAPIKey', ApiType.SYSAPI, {
            apiKey,
        });
    }

    /**
     * Send HTTP request.
     * Description: http://localhost:6128/SendRequest.html
     * @param address ip or dns name.
     * @param port port.
     * @param request request.
     */
    async sendRequest(address: string, port: string, request: string) {
        const response = await this.executeCommand<{ response: string }>(
            'SendRequest',
            ApiType.SYSAPI,
            {
                address,
                port,
                request,
            }
        );
        return response.response;
    }

    /**
     * Get profile info.
     * Description: http://localhost:6128/GetProfInfo.html
     * @param profile target profile.
     * @param password password for profile.
     */
    async getProfileInfo(profile: string, password: string) {
        const response = await this.executeCommand<{
            name: string;
            dev: string;
            user: string;
            param: string;
            cont: string;
            sn: string;
        }>('GetProfInfo', ApiType.BaseAPI, {
            profile,
            pass: password,
        });

        return {
            profileName: response.name,
            deviceType: response.dev,
            userName: response.user,
            params: response.param,
            containerType: response.cont,
            deviceSerialNumber: response.sn,
        };
    }

    /**
     * Load key from file and return certificates from key.
     * Description: http://localhost:6128/LoadKeyFromFile.html
     * @param pathToKey path om pc to key. (use "showOpenDialog" for get path on user pc)
     * @param password password for unlock key.
     */
    async loadKeyFromFile(pathToKey: string, password: string) {
        const response = await this.executeCommand<any>(
            'LoadKeyFromFile',
            ApiType.BaseAPI,
            {
                pathKey: pathToKey,
                pass: password,
            }
        );
        let certificates: Certificate[] = [];
        let certificateIndex = 0;
        while (response.hasOwnProperty(`certificate${certificateIndex}`)) {
            const certificate = response[
                `certificate${certificateIndex}`
            ] as Certificate;
            certificateIndex++;
            certificates.push(certificate);
        }
        return certificates;
    }

    /**
     * Load key from profile and return certificates.
     * Description: http://localhost:6128/LoadKeyFromProfile.html
     * @param profile target profile.
     * @param password password for unlock profile.
     */
    async loadKeyFromProfile(profile: string, password?: string) {
        const response = await this.executeCommand<any>(
            'LoadKeyFromProfile',
            ApiType.BaseAPI,
            {
                profile: profile,
                pass: password,
            }
        );
        let certificates: Certificate[] = [];
        let certificateIndex = 0;
        while (response.hasOwnProperty(`certificate${certificateIndex}`)) {
            const certificate = response[
                `certificate${certificateIndex}`
            ] as Certificate;
            certificateIndex++;
            certificates.push(certificate);
        }
        return certificates;
    }

    /**
     * Load key from token and return certificates.
     * Description: http://localhost:6128/LoadKeyFromTokens.html
     * @param pluginName token provider.
     * @param password password for unlock token.
     */
    async loadKeyFromTokens(pluginName?: 'kztoken', password?: string) {
        const response = await this.executeCommand<any>(
            'LoadKeyFromTokens',
            ApiType.BaseAPI,
            { plugin_name: pluginName, pass: password }
        );
        let certificates: Certificate[] = [];
        let certificateIndex = 0;
        while (response.hasOwnProperty(`certificate${certificateIndex}`)) {
            const certificate = response[
                `certificate${certificateIndex}`
            ] as Certificate;
            certificateIndex++;
            certificates.push(certificate);
        }
        return certificates;
    }

    sign(
        profile?: string,
        password?: string,
        keySerialNumber?: string,
        detach?: boolean,
        isConvert?: boolean,
        isCert?: boolean,
        data?: string,
        hashType?: number,
        hash?: string,
        showSN?: boolean,
        transID?: string,
        cert?: string,
        ocsp?: string,
        tsp?: string,
        signattr?: SignAttribute[],
        unsignattr?: SignAttribute[],
        pkcs7?: string
    ) {
        return this.executeCommand<{ pkcs7: string }>('Sign', ApiType.BaseAPI, {
            profile,
            pass: password,
            sn: keySerialNumber,
            detach,
            isConvert,
            isCert,
            data,
            hashType,
            hash,
            showSN,
            transID,
            cert,
            ocsp,
            tsp,
            signattr,
            unsignattr,
            pkcs7,
        });
    }
    /**
     * Get all keys.
     * Description: http://localhost:6128/ShowOpenDialog.html
     * @param url Url to keys,
     */
    async getAllKeys(url?: string) {
        const response = await this.executeCommand<{
            response: { profiles: string };
        }>('GetAllKeys', ApiType.BaseAPI, {
            url,
        });
        return response.response.profiles;
    }

    /**
     * Show file selector on user pc with custom title.
     * Description: http://localhost:6128/ShowOpenDialog.html
     * @param title text on window title.
     */
    async showOpenDialog(title: string) {
        const response = await this.executeCommand<{ path: string }>(
            'ShowOpenDialog',
            ApiType.FORMAPI,
            {
                title,
            }
        );
        return response.path;
    }

    /**
     * Create PKCS10.
     * Description: http://localhost:6128/CreatePKCS10.html
     */
    async createPKCS10(
        profile: string,
        dn: string,
        templ: string,
        isExport: string,
        genNew: string,
        keyType: string,
        keySize: string,
        isAltName: string,
        hashType: string,
        password?: string,
        altEmail?: [],
        altDN?: [],
        altURI?: [],
        altREGID?: [],
        altIP?: [],
        altOther?: [],
        contName?: string
    ) {
        const response = await this.executeCommand<{ response: any }>(
            'CreatePKCS10',
            ApiType.FORMAPI,
            {
                profile,
                dn,
                templ,
                isExport,
                genNew,
                keyType,
                keySize,
                isAltName,
                hashType,
                password,
                altEmail,
                altDN,
                altURI,
                altREGID,
                altIP,
                altOther,
                contName,
            }
        );
        return response.response;
    }

    /**
     * Create certificate request.
     * Description: http://localhost:6128/CreateCertificateRequest.html
     */
    async createCertificateRequest(
        PKCS10: {
            profile: string;
            pass: string;
            dn: string;
            templ: string;
            isExport: string;
            genNew: string;
            keyType: string;
            keySize: string;
            isAltName: string;
            hashType: string;
            altEmail: string;
            altDN: string;
            altURI: string;
            altREGID: string;
            altIP: string;
            altOther: string;
            contName: string;
        },
        PKCS7: {
            profile: string;
            pass: string;
            sn: string;
            isCert: boolean;
            hashType: 0 | 1;
            showSN: boolean;
            cert: string;
        }
    ) {
        const response = await this.executeCommand<{ response: any }>(
            'CreateCertificateRequest',
            ApiType.BaseAPI,
            {
                PKCS10,
                PKCS7,
            }
        );
        return response.response;
    }

    /**
     * Install certificate.
     * Description: http://localhost:6128/InstallCertificate.html
     */
    async installCertificate(
        profile: string,
        password?: string,
        certificate?: string,
        flagCN?: number,
        rootCertificate?: string
    ) {
        const response = await this.executeCommand<{}>(
            'InstallCertificate',
            ApiType.BaseAPI,
            {
                profile,
                pass: password,
                certificate,
                flagCN,
                rootCertificate,
            }
        );
        return response.result;
    }

    /**
     * Get current transaction id.
     * Description: http://localhost:6128/GetTransactionID.html
     * @returns transaction id.
     */
    async getTransactionID() {
        const response = await this.executeCommand<{ response: string }>(
            'GetTransactionID',
            ApiType.BaseAPI,
            {}
        );
        return response.response;
    }

    /**
     * Get all certificates.
     * Description: http://localhost:6128/GetAllCertificate.html
     */
    async getAllCertificate() {
        const response = await this.executeCommand<{ response: any }>(
            'GetAllCertificate',
            ApiType.BaseAPI,
            {}
        );
        return response.response;
    }
    // TODO: Fix comments usin convention
    /**
     * Create CMP request for produce certificate.
     * Description: http://localhost:6128/initialRegistrationRequest.html
     * @param profile Profile to key.
     * @param uid User UID.
     * @param secret Secret.
     * @param password Password for profile.
     * @param keyOID List of OID for key generation.
     * @param altEmail Alternative names in "email" format.
     * @param altDN Alternative names in "Distinguished Name" format.
     * @param altURI Alternative names in "URL" format.
     * @param altREGID Alternative names in "Registration identifier" format.
     * @param altIP Alternative names in "IP" format.
     */
    async initialRegistrationRequest(
        profile: string,
        uid: string,
        secret: string,
        password?: string,
        keyOID?: string,
        altEmail?: string,
        altDN?: string,
        altURI?: string,
        altREGID?: string,
        altIP?: string
    ) {
        const response = await this.executeCommand<{ response: any }>(
            'initialRegistrationRequest',
            ApiType.BaseAPI,
            {
                profile,
                uid,
                secret,
                pass: password,
                keyOID,
                altEmail,
                altDN,
                altURI,
                altREGID,
                altIP,
            }
        );
        return response.response;
    }

    /**
     * Get status of CMP process IR, CR and install certificates in container.
     * Description: http://localhost:6128/initialRegistrationRequest.html
     * @param profile Profile to key.
     * @param secret Secret.
     * @param response response from initialRegistrationRequest.
     * @param password Password for profile.
     */
    async initialRegistrationResponse(
        profile: string,
        secret: string,
        response: string,
        password?: string
    ) {
        const resp = await this.executeCommand<{
            failCode: string;
            failerror: string;
            time: string;
            status: string;
        }>('initialRegistrationRequest', ApiType.BaseAPI, {
            profile,
            secret,
            pass: password,
            response,
        });
        return resp;
    }

    /**
     * Create CMP request for check transaction.
     * Description: http://localhost:6128/initialRegistrationRequest.html
     * @param profile Profile to key.
     * @param transactionId Transaction id.
     * @param password Password for profile.
     * @param genOid OID of extended message.
     * @param cert Server certificate.
     * @param uid User UID.
     * @param secret Secret.
     * @returns CMP PR response.
     */
    async pollingRequest(
        profile: string,
        transactionId: string,
        password?: string,
        genOid?: string,
        cert?: string,
        uid?: string,
        secret?: string
    ) {
        const resp = await this.executeCommand<{
            response: any;
        }>('pollingRequest', ApiType.BaseAPI, {
            profile,
            transactionId,
            pass: password,
            genOid,
            cert,
            uid,
            secret,
        });
        return resp.response;
    }

    /**
     * Get process status of CMP request and install certificates in container.
     * Description: http://localhost:6128/initialRegistrationRequest.html
     * @param profile Profile to key.
     * @param response Response from "pollingRequest".
     * @param password Password for profile.
     * @param processResponse true for try install certificates on pc if returned response PKI_CMP_IP or PKI_CMP_СP.
     * @param secret Secret.
     * @returns CMP PR response.
     */
    async pollingResponse(
        profile: string,
        response: string,
        password?: string,
        processResponse?: boolean,
        secret?: string
    ) {
        const resp = await this.executeCommand<{
            failcode: string;
            failerror: string;
            transID: string;
            checkAfter: string;
            time: string;
            status: string;
            certs: Certificate[];
        }>('pollingResponse', ApiType.BaseAPI, {
            profile,
            response,
            pass: password,
            processResponse,
            secret,
        });
        return resp;
    }

    /**
     * Create OCSP request for certificate.
     * Description: http://localhost:6128/createOCSPRequest.html
     * @param profile Profile to key.
     * @param certificate Certificate.
     * @returns OCSP request.
     */
    async createOCSPRequest(profile: string, certificate: string) {
        const resp = await this.executeCommand<{
            ocsp: string;
        }>('createOCSPRequest', ApiType.BaseAPI, {
            profile,
            cert: certificate,
        });
        return resp.ocsp;
    }

    /**
     * Get info from OCSP response.
     * Description: http://localhost:6128/parseOCSPResponce.html
     * @param response OCSP response.
     * @returns Info from OCSP response.
     */
    async parseOCSPResponce(response: string) {
        return await this.executeCommand<{
            statusStr: string;
            timeCreate: string;
            snCert: string;
            status: string;
            timeRevoke: string;
            reasonRevoke: string;
        }>('parseOCSPResponce', ApiType.BaseAPI, {
            response,
        });
    }

    /**
     * Create CMP request for recreate certificate.
     * Description: http://localhost:6128/certificationRequest.html
     * @param profile Profile to key.
     * @param password Password to key.
     * @param keyOID OIDs for key generation.
     * @param altEmail Alternative names in "email" format.
     * @param altDN Alternative names in "Distinguished Name" format.
     * @param altURI Alternative names in "URL" format.
     * @param altREGID Alternative names in "Registration identifier" format.
     * @param altIP Alternative names in "IP" format.
     * @returns CMP request.
     */
    async certificationRequest(
        profile: string,
        password?: string,
        keyOID?: string,
        altEmail?: string,
        altDN?: string,
        altURI?: string,
        altREGID?: string,
        altIP?: string
    ) {
        return await this.executeCommand<{
            response: any;
        }>('certificationRequest', ApiType.BaseAPI, {
            profile,
            pass: password,
            keyOID,
            altEmail,
            altDN,
            altURI,
            altREGID,
            altIP,
        });
    }

    /**
     * Create request for get transactions list.
     * Description: http://localhost:6128/transactionListRequest.html
     * @param profile Profile to key.
     * @param password Password to key.
     * @param certificate OServer certificate.
     * @returns CMP CR request.
     */
    async transactionListRequest(
        profile: string,
        password?: string,
        certificate?: string
    ) {
        return await this.executeCommand<{
            response: any;
        }>('transactionListRequest', ApiType.BaseAPI, {
            profile,
            pass: password,
            cert: certificate,
        });
    }

    /**
     * Check request for get transactions list.
     * Description: http://localhost:6128/transactionListResponse.html
     * @param profile Profile to key.
     * @param response Response from server.
     * @param password Password to key.
     * @returns response contains list of transactions in base64 format.
     */
    async transactionListResponse(
        profile: string,
        response: string,
        password?: string
    ) {
        return await this.executeCommand<{
            failcode: string;
            failerror: string;
            response: string;
        }>('transactionListRequest', ApiType.BaseAPI, {
            profile,
            pass: password,
            trresp: response,
        });
    }

    /**
     * Create request for add / edit / delete secret.
     * Description: http://localhost:6128/createSecretRequest.html
     * @param profile Profile to key.
     * @param serverCert Server certificate.
     * @param type Request type (0 - add secret, 1 - edit secret, 2 - delete secret).
     * @param password Password to key.
     * @param uid User ID.
     * @param secret Secret.
     * @param dn DN certificate name.
     * @param state User state.
     * @returns CMP SR request.
     */
    async createSecretRequest(
        profile: string,
        serverCert: string,
        type: 0 | 1 | 2,
        password?: string,
        uid?: string,
        secret?: string,
        dn?: string,
        state?: string
    ) {
        return await this.executeCommand<{
            response: any;
        }>('createSecretRequest', ApiType.BaseAPI, {
            profile,
            serverCert,
            type,
            pass: password,
            uid,
            secret,
            dn,
            state,
        });
    }

    /**
     * Check request for add / edit / delete secret.
     * Description: http://localhost:6128/createSecretResponse.html
     * @param profile Profile to key.
     * @param resp Response from "createSecretRequest".
     * @param password Password to key.
     * @returns CMP SR request.
     */
    async createSecretResponse(
        profile: string,
        resp: string,
        password?: string
    ) {
        return await this.executeCommand<{
            failcode: string;
            failerror: string;
        }>('createSecretResponse', ApiType.BaseAPI, {
            profile,
            resp,
            pass: password,
        });
    }

    /**
     * Create request for get server certificate, COC.
     * Description: http://localhost:6128/x509ObjectRequest.html
     * @param profile Profile to key.
     * @param password Password to key.
     * @param cert Server certificate.
     * @param crl Flag for request CRL.
     * @returns CMP request.
     */
    async x509ObjectRequest(
        profile: string,
        password?: string,
        cert?: string,
        crl?: string
    ) {
        return await this.executeCommand<{
            response: any;
        }>('x509ObjectRequest', ApiType.BaseAPI, {
            profile,
            pass: password,
            cert,
            crl,
        });
    }

    /**
     * Check request for get server certificate, COC.
     * Description: http://localhost:6128/x509objectResponse.html
     * @param profile Profile to key.
     * @param response Server certificate.
     * @param password Password to key.
     * @returns CMP request.
     */
    async x509objectResponse(
        profile: string,
        response: string,
        password?: string,
        crl?: string
    ) {
        return await this.executeCommand<{
            failcode: string;
            failerror: string;
            response: any;
        }>('x509objectResponse', ApiType.BaseAPI, {
            profile,
            resp: response,
            pass: password,
            crl,
        });
    }

    /**
     * Create request for save/get keys.
     * Description: http://localhost:6128/certificateConfirmationRequest.html
     * @param profile Profile to key.
     * @param pollingResponse Response on get transaction status (If request postponed).
     * @param confirm Confirm or refuse transaction.
     * @param password Password to key.
     * @param cert Server certificate.
     * @returns CMP request.
     */
    async certificateConfirmationRequest(
        profile: string,
        pollingResponse: string,
        confirm: boolean,
        password?: string,
        cert?: string
    ) {
        return await this.executeCommand<{
            otp: string;
            response: any;
        }>('certificateConfirmationRequest', ApiType.BaseAPI, {
            profile,
            pollingResponse,
            confirm,
            pass: password,
            cert,
        });
    }

    /**
     * Check response for save/get keys.
     * Description: http://localhost:6128/certificateConfirmationResponse.html
     * @param profile Profile to key.
     * @param response response from "certificateConfirmationRequest".
     * @param password Password to key.
     * @returns CMP request.
     */
    async certificateConfirmationResponse(
        profile: string,
        response: string,
        password?: string
    ) {
        return await this.executeCommand<{
            otp: string;
            response: any;
        }>('certificateConfirmationResponse', ApiType.BaseAPI, {
            profile,
            pass: password,
            resp: response,
        });
    }

    /**
     * Create request for revoce certificate.
     * Description: http://localhost:6128/revocationRequest.html
     * @param profile Profile to key.
     * @param reason Revocation reason.
     * @param confirm Confirm or refuse transaction.
     * @param password Password to key.
     * @param cert Server certificate.
     * @returns CMP request.
     */
    async revocationRequest(
        profile: string,
        reason: RevocationReason,
        password?: string,
        sn?: string,
        userCertificate?: string,
        uid?: string,
        userPassword?: string,
        serverCert?: string
    ) {
        return await this.executeCommand<{
            otp: string;
            response: any;
        }>('revocationRequest', ApiType.BaseAPI, {
            profile,
            reason,
            sn,
            userCertificate,
            uid,
            userPassword,
            serverCert,
            confirm,
            pass: password,
        });
    }

    /**
     * Check response for revoce certificate.
     * Description: http://localhost:6128/revocationResponse.html
     * @param profile Profile to key.
     * @param response response from "revocationRequest".
     * @param password Password to key.
     */
    async revocationResponse(
        profile: string,
        response: string,
        password?: string
    ) {
        return await this.executeCommand<{
            failcode: string;
            failerror: string;
            transId: string;
            type: string;
            status: string;
        }>('revocationResponse', ApiType.BaseAPI, {
            profile,
            resp: response,
            pass: password,
        });
    }

    /**
     * Get identity info from "Id card", connected to pc.
     * Description: http://localhost:6128/getIdCardInfo.html
     */
    async getIdCardInfo() {
        const response = await this.executeCommand<{
            response: { [key: string]: IdCardInfo };
        }>('getIdCardInfo', ApiType.BaseAPI, {});

        return Object.keys(response.response).map((x) => response.response[x]);
    }

    /**
     * Create encrypted PKCS7 envelope.
     * @param profile Profile to key.
     * @param password Password to key.
     * @param algId Encryption algorithm.
     * @param Flags Flag, if 0 - exclude source data.
     * @param data Data for encryption.
     * @param isConvert Decode data from base64 before encrypt.
     * @param RecipCerts Array of recipients certificates.
     */
    createEnvelopedPKCS7(
        profile?: string,
        password?: string,
        algId?: any,
        Flags?: number,
        data?: boolean,
        isConvert?: boolean,
        RecipCerts?: string[]
    ) {
        return this.executeCommand<{ response: string }>(
            'CreateEnvelopedPKCS7',
            ApiType.BaseAPI,
            {
                profile,
                pass: password,
                algId,
                Flags,
                data,
                isConvert,
                RecipCerts,
            }
        );
    }

    /**
     * Load profile from tokens.
     * @param pluginName Cryptoprovider plugin name.
     */
    async loadProfileFromTokens(pluginName?: 'kztoken') {
        const response = await this.executeCommand<{
            response: { profile: string; reader: string }[];
        }>('LoadProfileFromTokens', ApiType.BaseAPI, {
            plugin_name: pluginName,
        });
        return response.response;
    }

    /**
     * Verify sign in PKCS7 format.
     * @param isConvert Decode data from base64 before decrypt.
     * @param data Data for decryption.
     * @param hashType hash type for rsa certificates.
     * @param cert User certificate in base64, if not provided use certificate from pkcs7.
     * @param sign Verify target PKCS7.
     */
    verifyPKCS7(
        isConvert: boolean,
        data: string,
        hashType: HashType,
        cert?: string,
        sign?: any
    ) {
        return this.executeCommand<{}>('VerifyPKCS7', ApiType.BaseAPI, {
            isConvert,
            data,
            hashType,
            cert,
            sign,
        });
    }

    /**
     * Decrypt message in PKCS7 format.
     * @param data Message for decryption.
     * @param isConvert Decode data from base64 before decrypt.
     * @param profile Profile to key.
     * @param password Password to key.
     * @param isElGamal use scheme by alg ElGamal.
     * @param SenderCert Sender certificate.
     */
    async decryptEnvelopedPKCS7(
        data: string,
        isConvert: boolean,
        profile?: string,
        password?: string,
        isElGamal?: boolean,
        SenderCert?: any
    ) {
        const response = await this.executeCommand<{ response: string }>(
            'DecryptEnvelopedPKCS7',
            ApiType.BaseAPI,
            {
                isConvert,
                data,
                profile,
                pass: password,
                isElGamal,
                SenderCert,
            }
        );
        return response.response;
    }

    /**
     * Delete key and certificate.
     * @param profile Profile to key.
     * @param password Password to key.
     * @param keySerialNumber Key serial number.
     */
    async deleteKey(
        profile?: string,
        password?: string,
        keySerialNumber?: string
    ) {
        await this.executeCommand('DelKey', ApiType.BaseAPI, {
            profile,
            pass: password,
            sn: keySerialNumber,
        });
    }

    /**
     * Create clear sign.
     * @param profile Profile to key.
     * @param isConvert Decode data from base64 before sign.
     * @param password Password to key.
     * @param serialNumber Serial number.
     * @param data Data for sign (if provided hash param has been ignored).
     * @param hash Hash for sign.
     * @param hashType Type of hash.
     */
    async nativeSign(
        profile: string,
        isConvert: boolean,
        password?: string,
        serialNumber?: string,
        data?: string,
        hash?: HashType,
        hashType?: string
    ) {
        const response = await this.executeCommand<{ response: string }>(
            'NativeSign',
            ApiType.BaseAPI,
            {
                profile,
                isConvert,
                data,
                hash,
                hashType,
                pass: password,
                sn: serialNumber,
            }
        );
        return response.response;
    }

    /**
     * Verify clear sign.
     * @param isConvert Decode data from base64 before verify.
     * @param certificate Certificate.
     * @param data Data for verify (if provided hash param has been ignored).
     * @param hash Hash for sign.
     * @param hashType Type of hash.
     * @param signatureSignature for verify.
     */
    async verifyNative(
        isConvert: boolean,
        certificate?: string,
        data?: string,
        hash?: HashType,
        hashType?: string,
        signature?: string
    ) {
        await this.executeCommand('VerifyNative', ApiType.BaseAPI, {
            isConvert,
            cert: certificate,
            sign: signature,
            data,
            hash,
            hashType,
        });
    }

    /**
     * Load key from blob.
     * @param blob Blob file with keys in base64 format. (Allow using keys in PKCS12, PFX formats)
     * @param password Password to key.
     * @param sessionId Session id (if not provided generated by key).
     */
    async loadKeyFromBlob(blob: string, password?: string, sessionId?: string) {
        await this.executeCommand<{ data: { [key: string]: Certificate } }>(
            'LoadKeyFromBlob',
            ApiType.BaseAPI,
            {
                blob,
                pass: password,
                session_id: sessionId,
            }
        );
    }

    /**
     * Hash data.
     * @param blob Data for hash.
     * @param isConvert Decode blob from base64 before hash.
     * @param algorithmId hash algorithm.
     * @param format Format results view.
     * @param alf Alphabet for print on screen. (Used only for format=HashFormat.ALF)
     */
    async createHash(
        blob: string,
        isConvert: string,
        algorithmId: HashAlgorithm,
        format: HashFormat,
        alphabet: string
    ) {
        const response = await this.executeCommand<{ hash: string }>(
            'CreateHash',
            ApiType.BaseAPI,
            {
                blob,
                isConvert,
                algID: algorithmId,
                format,
                alf: alphabet,
            }
        );
        return response.hash;
    }

    /**
     * Encrypt or decrypt data by algorithm GOST 28147-89.
     * @param isCrypt
     * * true - Encrypt
     * * false - Decrypt.
     * @param secret Secret for encryption.
     * @param hashType Type of hash.
     * @param isConvert Decode blob from base64 before hash.
     * @param useSalt Use salt (if not defined use random salt).
     * @param salt Salt for hash.
     * @param filename path to file for hash. (If defined used instead data property)
     */
    async nativeCrypt(
        isCrypt: boolean,
        secret: string,
        hashType: HashType,
        isConvert: boolean,
        useSalt: boolean,
        salt?: string,
        filename?: string,
        data?: string
    ) {
        const response = await this.executeCommand<{
            response: string;
            salt: string;
        }>('NativeCrypt', ApiType.BaseAPI, {
            isCrypt,
            secret,
            hashType,
            isConvert,
            useSalt,
            salt,
            filename,
            data,
        });
        return {
            response: response.response,
            salt: response.salt,
        };
    }

    /**
     * Verify certificate sign.
     * @param certificate User certificate in bse64.
     * @param secret Root certificate in bse64.
     */
    async checkCertificate(certificate: string, rootCertificate: string) {
        await this.executeCommand('CheckCertificate', ApiType.BaseAPI, {
            certificate,
            rootCertificate,
        });
    }

    /**
     * Create TSP request.
     * @param hashType Hash algorithm.
     * @param data Data for timestamp mark.
     * @param isConvert Decode data fro mbase64 before hash.
     * @param policy TSP policy.
     * @param includeServerCert Include server certificate in response.
     * @returns TSP request in asn1 format (base64).
     */
    async createTSPRequest(
        hashType: HashFormat,
        data: string,
        isConvert: boolean,
        policy: string,
        includeServerCert = false
    ): Promise<string> {
        const response = await this.executeCommand<{ tsp: string }>(
            'createTSPRequest',
            ApiType.BaseAPI,
            {
                hashType,
                data,
                isConvert,
                policy,
                includeServerCert,
            }
        );
        return response.tsp;
    }

    /**
     * Get data in pem format.
     * @param profile Key profile.
     * @param password Password of key.
     * @param serialNumber Serial number.
     * @param exportData Type of exported data.
     * @param exportPass Password for export key.
     */
    async getPemFormat(
        profile: string,
        password: string,
        serialNumber: string,
        exportData: ExportData,
        exportPass = false
    ) {
        const response = await this.executeCommand<{ data: string }>(
            'getPemFormat',
            ApiType.BaseAPI,
            {
                profile,
                pass: password,
                sn: serialNumber,
                exportData,
                exportPass,
            }
        );
        return response.data;
    }

    /**
     * Change password on profile.
     * @param profile Key profile.
     * @param password Password of key.
     * @param newPassword New password.
     */
    async changePassword(
        profile: string,
        password: string,
        newPassword: string
    ) {
        await this.executeCommand('changePassword', ApiType.BaseAPI, {
            profile,
            password,
            newPassword,
        });
    }

    /**
     * Export data to file.
     * @param profile Key profile.
     * @param serialNumber New password.
     * @param exportData New password.
     * @param title New password.
     * @param password Password of key.
     * @param exportPassword New password.
     */
    async exportDataToFile(
        profile: string,
        serialNumber: string,
        exportData: ExportDataToFileType,
        title: string,
        password?: string,
        exportPassword?: string
    ) {
        await this.executeCommand('exportDataToFile', ApiType.BaseAPI, {
            profile,
            pass: password,
            sn: serialNumber,
            exportData,
            title,
            exportPass: exportPassword,
        });
    }

    /**
     * Export data to file.
     * @param profile Key profile.
     * @param xml XML document for sign in BASE64.
     * @param serialNumber New password.
     * @param keyOid OID of key for sign.
     * @param nsPref NS prefix for sign tag.
     * @param password Password of key.
     */
    async signXML(
        profile: string,
        xml: string,
        serialNumber?: string,
        keyOid?: string,
        nsPref?: string,
        password?: string
    ) {
        const response = await this.executeCommand<{ signxml: string }>(
            'SignXML',
            ApiType.XMLAPI,
            {
                profile,
                xml,
                sn: serialNumber,
                keyOid,
                nsPref,
                pass: password,
            }
        );
        return response.signxml;
    }

    /**
     * Verify sign  on document in format XMLDSig.
     * @param xml XML document for verify in BASE64.
     */
    async verifyXML(xml: string) {
        await this.executeCommand('VerifyXML', ApiType.XMLAPI, {
            xml,
        });
    }

    /**
     * Create signature on SOAP document in XMLDSig format.
     * @param profile Key profile.
     * @param xml XML document for sign in BASE64.
     * @param serialNumber Serial number for revoked certificate.
     * @param keyOid OID of key for sign.
     * @param idElement Id of element for sign.
     */
    async signSOAPXML(
        profile: string,
        xml: string,
        password?: string,
        serialNumber?: string,
        keyOid?: string,
        idElement?: string
    ) {
        const response = await this.executeCommand<{ signxml: string }>(
            'SignSOAPXML',
            ApiType.XMLAPI,
            {
                profile,
                xml,
                pass: password,
                sn: serialNumber,
                keyOid,
                idElement,
            }
        );
        return response.signxml;
    }

    /**
     * Verify API key.
     * @param apiKey API Key for verify.
     */
    async checkAPIKey(apiKey: string) {
        await this.executeCommand('CheckAPIKey', ApiType.SYSAPI, {
            apiKey,
        });
    }

    /**
     * Check on update.
     * @param address Address to update server.
     * @param port Port to update server.
     * @param url Path to update on erver.
     */
    async update(address: string, port: string, url: string) {
        await this.executeCommand('Update', ApiType.SYSAPI, {
            address,
            port,
            url,
        });
    }

    /**
     * Get fingerprint.
     * @param timeout Time for await fingerprint in ms. (default 1 min)
     * @param viewdlg If true - show standard dialog.
     */
    async getFingerprint(timeout: number, viewdlg: number) {
        const response = await this.executeCommand<{ fingerprint: string }>(
            'GetFingerprint',
            ApiType.SYSAPI,
            {
                timeout,
                viewdlg,
            }
        );
        return response.fingerprint;
    }

    /**
     * Check fingerprint.
     * @param timeout Time for await fingerprint in ms. (default 1 min)
     * @param viewdlg If true - show standard dialog.
     * @param fingerprint Fingerprint.
     */
    async checkFingerprint(
        timeout: number,
        viewdlg: number,
        fingerprint: string
    ) {
        await this.executeCommand('CheckFingerprint', ApiType.SYSAPI, {
            timeout,
            viewdlg,
            fingerprint,
        });
    }

    /**
     * Install fingerprint on token.
     * @param pin PIN codefor access to device.
     * @param timeout Time for await fingerprint in ms. (default 1 min)
     * @param viewdlg If true - show standard dialog.
     * @param name Name for fingerprint. (max 20 letters) (KAZTOKEN UNSUPPORTED)
     * @param index ONLY FOR KAZTOKEN.
     */
    async installFingerprintToken(
        pin: string,
        timeout?: number,
        viewdlg?: number,
        name?: string,
        index?: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10
    ) {
        await this.executeCommand('InstallFingerprintToken', ApiType.SYSAPI, {
            pin,
            timeout,
            viewdlg,
            name,
            indx: index,
        });
    }

    /**
     * Check fingerprint on token.
     * @param pin PIN codefor access to device.
     * @param timeout Time for await fingerprint in ms. (default 1 min)
     * @param viewdlg If true - show standard dialog.
     * @param name Name for fingerprint. (max 20 letters) (KAZTOKEN UNSUPPORTED)
     * @param index ONLY FOR KAZTOKEN.
     */
    async checkFingerprintFromToken(
        pin: string,
        timeout?: number,
        viewdlg?: number,
        name?: string,
        index?: 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10
    ) {
        const response = await this.executeCommand<{
            name: string;
            indx: string;
        }>('CheckFingerprintFromToken', ApiType.SYSAPI, {
            pin,
            timeout,
            viewdlg,
            name,
            indx: index,
        });
        return {
            name: response.name,
            index: response.indx,
        };
    }

    /**
     * Get info about mac addresses of network devices.
     * @param isIp Show info about ip addresses.
     */
    async getMACAddress(isIp?: boolean) {
        const response = await this.executeCommand<{
            macaddr: string;
        }>('GetMACAddress', ApiType.SYSAPI, {
            isIp,
        });
        return response.macaddr;
    }

    /**
     * Get fingerprint and save on device.
     * @param device Device name for record fingerprint.
     * @param pin for access to fingerprint device.
     * @param dn DN username.
     * @param profile Profile for sign.
     * @param password Password of profile.
     * @param serialNumber Serial number of key in HEX.
     * @param name Name of fingerprint. (Max 20 letters)
     * @param indx Token index. (ONLY FOR KATOKENS)
     * @param timeout Time for await fingerprint in ms. (default 1 min)
     * @param viewdlg If true - show standard dialog.
     */
    async installFingerprintTokenAndSign(
        device: string,
        pin: string,
        dn: string,
        profile: string,
        password: string,
        serialNumber?: string,
        name?: string,
        indx?: number,
        timeout?: number,
        viewdlg?: number
    ) {
        const response = await this.executeCommand<{
            hash: string;
        }>('InstallFingerprintTokenAndSign', ApiType.SYSAPI, {
            device,
            pin,
            dn,
            profile,
            pass: password,
            sn: serialNumber,
            name,
            indx,
            timeout,
            viewdlg,
        });
        return response.hash;
    }

    /**
     * Get list of signed fingerprints.
     * @param device Device name with recorded fingerprint.
     * @param pin for access to device.
     * @param timeout Time for await fingerprint in ms. (default 1 min)
     * @param viewdlg If true - show standard dialog.
     */
    async listFingerprintFromToken(
        device: string,
        pin: string,
        timeout?: number,
        viewdlg?: number
    ) {
        const response = await this.executeCommand<{
            fingers: { [key: string]: FingerprintInfo };
        }>('ListFingerprintFromToken', ApiType.SYSAPI, {
            device,
            pin,
            timeout,
            viewdlg,
        });
        return response.fingers;
    }

    /**
     * Get info about saved signed fingerprints.
     * @param device Device name with recorded fingerprint.
     * @param pin for access to device.
     * @param certificate Certificate for verify sign.
     * @param timeout Time for await fingerprint in ms. (default 1 min)
     * @param viewdlg If true - show standard dialog.
     */
    async checkFingerprintAndSignFromToken(
        device: string,
        pin: string,
        certificate: string,
        timeout?: number,
        viewdlg?: number
    ) {
        const response = await this.executeCommand<{
            name: string;
            indx: string;
            hash: string;
        }>('CheckFingerprintAndSignFromToken', ApiType.SYSAPI, {
            device,
            pin,
            cert: certificate,
            timeout,
            viewdlg,
        });
        return {
            name: response.name,
            index: response.indx,
            hash: response.hash,
        };
    }

    /**
     * Get info about environment variables.
     * @param nameEnv Environment name.
     */
    async getEnvironment(nameEnv?: string) {
        const response = await this.executeCommand<{
            env: any[];
        }>('GetEnvironment', ApiType.SYSAPI, {
            nameEnv,
        });
        return response.env;
    }

    /**
     * Crypt message.
     * @param profile Profile for sign.
     * @param password Password of profile.
     * @param dataDescriptor Data description.
     * @param dataOID OID for descripted signed data.
     * @param cryptOID Encryption algorithm.
     * @param ownerCertificate Own certificate.
     * @param Flags if 1 - include sender certificate.
     * @param data Data for encryption.
     * @param isConvert Decode data fro mbase64 before hash.
     * @param RecipCerts Recipients certificates.
     */
    async cryptMessage(
        dataDescriptor: string,
        dataOID: string,
        cryptOID: string,
        ownerCertificate: string,
        Flags: number,
        data: string,
        isConvert: boolean,
        recipCerts: string[],
        profile?: string,
        password?: string
    ) {
        const response = await this.executeCommand<{
            response: string;
        }>('CryptMessage', ApiType.ASNAPI, {
            dataDescriptor,
            dataOID,
            cryptOID,
            ownerCertificate,
            Flags,
            data,
            isConvert,
            RecipCerts: recipCerts,
            profile,
            password,
        });
        return response.response;
    }

    /**
     * Decrypt message.
     * @param profile Profile for sign.
     * @param password Password of profile.
     * @param data Data for decryption.
     * @param ownerCertificate Own certificate.
     * @param isConvert Decode data fro mbase64 before hash.
     * @param isElGamal use scheme by alg ElGamal.
     * @param senderCertificates Senders certificates.
     */
    async decryptMessage(
        profile: string,
        data: string,
        ownerCertificate: string,
        isConvert: boolean,
        isElGamal: string,
        password?: string,
        senderCertificates?: string[]
    ) {
        const response = await this.executeCommand<{
            response: string;
        }>('DecryptMessage', ApiType.ASNAPI, {
            isElGamal,
            ownerCertificate,
            data,
            isConvert,
            SenderCerts: senderCertificates,
            profile,
            password,
        });
        return response.response;
    }

    /**
     * Get info from CMS message.
     * @param data CMS data.
     * @param isConvert Decode data fro mbase64 before hash.
     */
    async showInfoMessage(data: string, isConvert: boolean) {
        const response = await this.executeCommand<{
            certInfo: { [key: string]: CertificateInfo };
            content: string;
            signedAttribute: string;
            unsignedAttribute: string;
            type: CmsType;
            senderName: string[];
            recipName: string[];
        }>('ShowInfoMessage', ApiType.ASNAPI, {
            data,
            isConvert,
        });
        return {
            infos: response.certInfo,
            content: response.content,
            signedAttribute: response.signedAttribute,
            unsignedAttribute: response.unsignedAttribute,
            type: response.type,
            senderName: response.senderName,
            recipName: response.recipName,
        };
    }

    /**
     * Get info from PKCS10 message.
     * @param data PKCS10.
     */
    async showInfoPKCS10(data: string) {
        const response = await this.executeCommand<{
            keyOID: string;
            publicKey: string;
        }>('ShowInfoPKCS10', ApiType.ASNAPI, {
            data,
        });
        return {
            keyOID: response.keyOID,
            publicKey: response.publicKey,
        };
    }

    /**
     * Get info from certificate.
     * @param certificate certificate.
     */
    async showInfoCertificate(certificate: string) {
        const response = await this.executeCommand<{
            data: CertificateInfo;
        }>('ShowInfoCertificate', ApiType.ASNAPI, {
            data: certificate,
        });
        return response.data;
    }

    /**
     * Show save file dialog.
     * @param title Dialog title.
     */
    async showSaveDialog(title: string) {
        const response = await this.executeCommand<{
            path: string;
        }>('ShowSaveDialog', ApiType.ASNAPI, {
            title,
        });
        return response.path;
    }

    /**
     * Internal wrapper for async call socket actions.
     * @param action Cryptosocket action.
     * @param params arguments for selected function.
     * @return Promise.
     */
    private executeCommand<T>(
        action: string,
        apiType: ApiType,
        params: any
    ): Promise<SuccessResponse & T> {
        return new Promise<SuccessResponse & T>((resolve, reject) => {
            if (this.socket === null) {
                return reject(CryptoSocketExceptions.NOT_INIT);
            }

            this.socket.onmessage = (event: MessageEvent) => {
                const response = JSON.parse(event.data);
                if (response.result === 'true') {
                    resolve(response);
                    return;
                }
                const error = mapSocketError(response);
                console.error({ action, params, error });
                reject(error);
                return;
            };
            var data = {
                TumarCSP: apiType,
                Function: action,
                Param: params,
            };
            this.socket.send(JSON.stringify(data));
        });
    }
}
