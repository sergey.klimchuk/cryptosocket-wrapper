export interface BaseResponse {
    result: 'true' | 'false';
    id?: string;
}

export interface SuccessResponse extends BaseResponse {
    result: 'true';
}

export interface BaseErrorResponse extends BaseResponse {
    result: 'false';
    code: string;
    error: string;
}

export interface APITokenExpiredErrorResponse extends BaseErrorResponse {
    code: '2148073504';
    time: string;
}

export interface InvalidKeyErrorResponse extends BaseErrorResponse {
    code: '-2146893807';
}

export interface WrongPasswordErrorResponse extends BaseErrorResponse {
    code: '2148073494' | '2148073488';
}

export interface APITokenErrorResponse extends BaseErrorResponse {
    code: '2148073478' | '2148073476' | '4' | '3' | '2';
}

export interface AbortErrorResponse extends BaseErrorResponse {
    code: '310';
}

export type ErrorResponse =
    | APITokenExpiredErrorResponse
    | InvalidKeyErrorResponse
    | WrongPasswordErrorResponse
    | APITokenErrorResponse
    | AbortErrorResponse;
