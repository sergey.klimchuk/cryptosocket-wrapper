import { CryptoSocketExceptions } from './crypto-socket-exceptions';
import { ErrorResponse } from './responses';
import { ErrorData } from './error-data';

export function mapSocketError(errorResponse: ErrorResponse): ErrorData | null {
    let errorData: any;
    let message: string;
    switch (errorResponse.code) {
        case '-2146893807':
            message = CryptoSocketExceptions.INVALID_KEY;
            break;
        case '2148073494':
        case '2148073488':
            message = CryptoSocketExceptions.WRONG_PASSWORD;
            break;
        case '2148073504':
            message = CryptoSocketExceptions.API_TOKEN_EXPIRED;
            if (!!errorResponse.time) {
                errorData = errorResponse.time;
            }
            break;
        case '2148073478':
        case '2148073476':
        case '4':
        case '3':
        case '2':
            message = CryptoSocketExceptions.API_TOKEN_ERROR;
            break;
        case '310':
            return null;
        default:
            message = CryptoSocketExceptions.UNKNOWN_ERROR;
            break;
    }
    return {
        message,
        code: errorResponse.code,
        data: errorData,
    };
}
